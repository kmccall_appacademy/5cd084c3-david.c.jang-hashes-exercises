# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  helper = Hash.new(0)
  str.split.each { |word| helper[word] = word.length }
  helper
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, _| older[k] = newer[k] }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  ans = Hash.new(0)
  word.chars { |letter| ans[letter] += 1 }
  ans
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  helper = Hash.new(0)
  arr.each { |value| helper[value] += 1 }
  helper.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  ans = Hash.new(0)
  numbers.each { |number| number.even? ? ans[:even] += 1 : ans[:odd] += 1 }
  ans
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  helper = Hash.new(0)
  string.chars.each { |char| helper[char] += 1 if vowels.include?(char) }
  helper.sort_by { |_, v| v }.reverse[0][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  ans = []
  students_with_birthdays = students.select { |_, v| v > 6 }.keys
  students_with_birthdays.each_with_index do |student, indx|
    until students_with_birthdays[indx + 1].nil? do
      ans << [student, students_with_birthdays[indx + 1]]
      indx += 1
    end
  end
  ans
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  helper = Hash.new(0)
  specimens.each { |kind| helper[kind] += 1 }
  number_of_species = helper.keys.count
  smallest_population_size = helper.sort_by { |_, v| v }[0][1]
  largest_population_size = helper.sort_by { |_, v| v }[-1][1]

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  helper = Hash.new(0)
  modified = normal_sign.downcase.delete(".,!@?><;: ")
  vandalized_sign.downcase.delete(".,!@?><;: ").chars.each { |ch| helper[ch] += 1 }
  helper.all? { |k, v| modified.count(k) >= v }
end
